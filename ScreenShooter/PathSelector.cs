﻿using DevExpress.Utils.CommonDialogs;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace ScreenShooter
{
    public class PathSelector
    {
        public static long id = 1;
        [System.ComponentModel.Browsable(true)]
        public String SelectedPath { get; set; }
        public static String SelectPath()
        {
            var browser = new XtraFolderBrowserDialog();
            browser.DialogStyle = FolderBrowserDialogStyle.Wide;
            browser.ShowDialog();
            return browser.SelectedPath;
        }
        public static void OpenFolder(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe"
                };
                Process.Start(startInfo);
            }
            else
            {
                XtraMessageBox.Show(string.Format("{0} Directory not found", folderPath));
            }
        }
        public static void SaveConfig(string path, string format, string theme)
        {
            string[] xpaths = { "path", "format", "theme" };
            string[] values = {path, format, theme};
             bool IsValid = ValidateXml("config.xml");
            if(!File.Exists("config.xml") || !IsValid)
            {     
                var xmlWriter = XmlWriter.Create("config.xml");
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("config");
                for(int i = 0; i<xpaths.Length; i++)
                {
                    xmlWriter.WriteStartElement(xpaths[i]);
                    xmlWriter.WriteString(values[i]);
                    xmlWriter.WriteEndElement();
                }
                //Closing tags
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Close();
            }
            else
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load("config.xml");
                for(int i=0; i<values.Length; i++)
                {
                    xmlDocument.SelectSingleNode("config/" + xpaths[i]).InnerText = values[i];
                }
                xmlDocument.Save("config.xml");
            }
      
        }
        public static List<String> LoadConfig()
        {
            string[] xpaths = { "path", "format", "theme" };
            XmlDocument xmlDocument = new XmlDocument();
            List<string> configList = new List<string>();
            try
            {
                xmlDocument.Load("config.xml");
                for(int i=0; i<xpaths.Length; i++)
                {
                    configList.Add(xmlDocument.SelectSingleNode("config/" + xpaths[i]).InnerText);
                }
                return configList;
            }
            catch
            {
                return configList;
            }
            
        }
       public static bool ValidateXml(string xml)
        {
            try
            {
                XDocument xd1 = new XDocument();
                xd1 = XDocument.Load(xml);
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static string GenerateFilename(string path,string format)
        {
            string newPath = path + id.ToString() + format;
            while(File.Exists(newPath))
            {
                id++;
                newPath = path + id.ToString() + format;
                
            }
            return id.ToString() + format;
                      
        }
        public static List<string> LoadFilesFromDirectory(string directory)
        {
            var extension = new List<string>() { ".png", ".bmp", ".jpg" };
            var files = Directory.EnumerateFiles(directory, "*.*", SearchOption.TopDirectoryOnly)
            .Where(s => extension.Any(e => e == Path.GetExtension(s)))
            .Select(Path.GetFileName);

            return files.ToList();
        }
    }
}
