﻿using DevExpress.Skins;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ScreenShooter
{
    public partial class ScreenShooter : DevExpress.XtraBars.ToolbarForm.ToolbarForm
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public ImageFormat FileFormat { get; set; }
        public Screen ChosenScreen { get; set; }
        public Dictionary<string, ImageFormat> AvailableFormats { get; set; }
        public ScreenShooter()
        {
            InitializeComponent();
            Setup();
        }

        private void BtnSelectPath_Click(object sender, EventArgs e)
        {
            string path = PathSelector.SelectPath();
            if (path.Length > 0)
            {
                PathEdit.Text = path;
                fileList.Items.Clear();
                fileList.Items.AddRange(PathSelector.LoadFilesFromDirectory(PathEdit.Text).ToArray());
            }
        }

        private void BtnOpenLoc_Click(object sender, EventArgs e)
        {
            PathSelector.OpenFolder(PathEdit.Text);
        }

        private void BtnMakeScreen_Click(object sender, EventArgs e)
        {
            if (!PathEdit.Text.EndsWith("\\")) PathEdit.Text += "\\";
            if (displayRadioGroup.SelectedIndex == 0) ChosenScreen = Screen.PrimaryScreen;
            else ChosenScreen = Screen.AllScreens[1];
            FilePath = PathEdit.Text;
            var format = FileFormatCb.SelectedItem.ToString();
            FileFormat = AvailableFormats[format];
            if (!string.IsNullOrWhiteSpace(EditFileName.Text))
            {
                FileName = EditFileName.Text + format;
                this.Visible = false;
                ScreenCapture.TakeScreenShot(ChosenScreen, FilePath, FileName, FileFormat);
                this.Visible = true;
            }
            else
            {
                FileName = PathSelector.GenerateFilename(PathEdit.Text, format);
                this.Visible = false;
                ScreenCapture.TakeScreenShot(ChosenScreen, FilePath, FileName, FileFormat);
                this.Visible = true;
            }
            fileList.Items.Add(FileName);
        }
        private void ScreenShooter_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(Directory.Exists(PathEdit.Text))
            {
                PathSelector.SaveConfig(PathEdit.Text,FileFormatCb.SelectedItem.ToString(),this.LookAndFeel.SkinName);
            }           
        }

        private void Setup()
        {
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            // Load config.xml
            List<string> configList = new List<string>();
            configList = PathSelector.LoadConfig();
            AvailableFormats = new Dictionary<string, ImageFormat>
            {
                { ".bmp", ImageFormat.Bmp },
                { ".jpg", ImageFormat.Jpeg },
                { ".png", ImageFormat.Png }
            };
            displayRadioGroup.Properties.Items.Add(new RadioGroupItem(0, "Primary"));
            displayRadioGroup.Properties.Items.Add(new RadioGroupItem(1, "Secondary"));
            displayRadioGroup.Properties.BorderStyle = BorderStyles.NoBorder;
            if (Screen.AllScreens.Length == 1)
            {
                displayRadioGroup.Properties.Items[1].Enabled = false;
            }
            // Set primary display as default
            displayRadioGroup.EditValue = 0;
            FileFormatCb.Properties.Items.AddRange(AvailableFormats.Keys);
            //Load defaults if something is wrong with .xml
            if (configList.Count != 3 || !AvailableFormats.ContainsKey(configList[1]))
            {
                PathEdit.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                FileFormatCb.SelectedIndex = 0;
                this.LookAndFeel.SkinName = "Metropolis Dark";
            }
            else
            {
                PathEdit.Text = configList[0];
                FileFormatCb.SelectedIndex = FileFormatCb.Properties.Items.IndexOf(configList[1]);
                this.LookAndFeel.SkinName = configList[2];
                themeSelector.ItemIndex = themeSelector.Strings.IndexOf(configList[2]);
            }
            fileList.Items.AddRange(PathSelector.LoadFilesFromDirectory(PathEdit.Text).ToArray());
            //fileList.Items.AddRange(PathSelector.LoadFilesFromDirectory(PathEdit.Text));

            this.KeyPreview = true;

            settingsBarList.Strings.AddRange(new[] { "Save layout as default", "Load layout", "Reset layout" });
            layoutControl1.SetDefaultLayout();
            LayoutTools.RestoreUserDefaultLayout(layoutControl1);
        }

        private void ThemeSelector_ListItemClick(object sender, DevExpress.XtraBars.ListItemClickEventArgs e)
        {
            this.LookAndFeel.SkinName = themeSelector.Strings[e.Index];
        }

        private void FileList_DoubleClick(object sender, EventArgs e)
        {
            string path = string.Format($"{PathEdit.Text}\\{fileList.SelectedItem}");
            if (File.Exists(path))
            {
                ProcessStartInfo Info = new ProcessStartInfo()
                {
                    FileName = "mspaint.exe",
                    WindowStyle = ProcessWindowStyle.Maximized,
                    Arguments = "\"" + path + "\"",
                };
                Process.Start(Info);
            }
            else
            {
                XtraMessageBox.Show("File not found","Warning",MessageBoxButtons.OK, MessageBoxIcon.Warning);
                fileList.Items.Clear();
                fileList.Items.AddRange(PathSelector.LoadFilesFromDirectory(PathEdit.Text).ToArray());
            }
        }

        private void ScreenShooter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (Directory.Exists(PathEdit.Text))
                {
                    fileList.Items.Clear();
                    fileList.Items.AddRange(PathSelector.LoadFilesFromDirectory(PathEdit.Text).ToArray());
                }
                else
                {
                    fileList.Items.Clear();
                }
            }
        }

        private void SettingsBarList_ListItemClick(object sender, DevExpress.XtraBars.ListItemClickEventArgs e)
        {
            if (e.Index == 0) LayoutTools.SaveLayoutAsDefault(layoutControl1);
            else if (e.Index == 1) LayoutTools.LoadLayoutFromXml(layoutControl1);
            else LayoutTools.RestoreDefaultLayout(layoutControl1);
        }
    }
}
