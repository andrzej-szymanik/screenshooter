﻿using DevExpress.XtraEditors;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;

namespace ScreenShooter
{
    class ScreenCapture
    {
        public static void TakeScreenShot(Screen screen, string path, string fileName, ImageFormat imageFormat)
        {
            try
            {
                string fullPath = path + fileName;
                using (Bitmap bitmap = new Bitmap(screen.Bounds.Width, screen.Bounds.Height))
                {
                    using (Graphics g = Graphics.FromImage(bitmap as Image))
                    {
                        Thread.Sleep(200);
                        g.CopyFromScreen(screen.Bounds.X, screen.Bounds.Y, 0, 0, screen.Bounds.Size);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                            bitmap.Save(fullPath, imageFormat);
                        }
                        else
                        {
                            if (!File.Exists(fullPath))
                            {
                                bitmap.Save(fullPath, imageFormat);
                            }
                            else
                            {
                                if (XtraMessageBox.Show("Do you want to overwrite this files?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.No)
                                {
                                    bitmap.Save(fullPath, imageFormat);
                                }
                            }
                        }
                        g.Dispose();
                    }
                    bitmap.Dispose();
                }
            }
            catch
            {
                XtraMessageBox.Show("You don't have permisson to save in this location.");
            }        
        }
    }
}
