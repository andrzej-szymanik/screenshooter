﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace ScreenShooter
{

    class LayoutTools
    {
        public static void LoadLayoutFromXml(LayoutControl layoutControl)
        {
            try
            {
                XtraOpenFileDialog openFileBrowser = new XtraOpenFileDialog();
                var path = AppDomain.CurrentDomain.BaseDirectory;
                openFileBrowser.InitialDirectory = path;
                openFileBrowser.ShowDialog();
                var filePath = openFileBrowser.FileName;

                if(!string.IsNullOrEmpty(filePath))
                {
                    if (IsLayoutConfigXml(filePath)) layoutControl.RestoreLayoutFromXml(filePath);
                    else XtraMessageBox.Show("Wrong file.", "Info", MessageBoxButtons.OK);
                }
            }
            catch
            {
                XtraMessageBox.Show("Couldn't open file.", "Info", MessageBoxButtons.OK);
            }
            
        }

        public static void RestoreUserDefaultLayout(LayoutControl layoutControl)
        {
            if (File.Exists("defaultLayout.xml")) layoutControl.RestoreLayoutFromXml("defaultLayout.xml");
            else RestoreDefaultLayout(layoutControl);
        }

        public static void RestoreDefaultLayout(LayoutControl layoutControl)
        {
            layoutControl.RestoreDefaultLayout();
        }

        public static void SaveLayoutAsDefault(LayoutControl layoutControl)
        {
            layoutControl.SaveLayoutToXml("defaultLayout.xml");
        }

        private static bool IsLayoutConfigXml(string path)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            return xmlDocument.OuterXml.Contains("LayoutControl");
        }
    }
}
